/*this project created by : Muhammad Wildan Nur Karim
  More information about this project -> https:google.com */

//THIS IS ACTIVITY FOR KEEPER
//DIFFERENT BETWEEN STRIKER AND KEEPER PROGRAM ARE ON THE "OnCameraFrame"

package e.wldn.bismillah;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.SurfaceView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import static org.opencv.imgproc.Imgproc.putText;
import static org.opencv.imgproc.Imgproc.rectangle;

public class KeeperMainActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String TAG = "TcpServerActivity";
    private ServerSocket TcpServerSocket = null;
    private Socket TcpSocket;
    private Calendar cal;
    private Button connectBt;
    private boolean connectOk = false;
    private boolean connectReq;
    private int connectState = 1;
    private long currentTimeMillis;
    private boolean dateON;
    private boolean hexMode;
    private boolean ipAdressON;
    private EditText ipEdit1;
    private EditText ipEdit2;
    private EditText ipEdit3;
    private EditText ipEdit4;
    private TextView ipText;
    private int lineNumber;
    private ArrayList<String> list;
    private String localAddress;
    private boolean localEchoON;
    private boolean markON;
    private ListView messageView;
    private tcpReadThread mtcpReadThread;
    private EditText portEdit;
    private int posi;
    private ArrayAdapter<String> recieveArrayAdapter;
    private String remoteAddress;
    private CheckBox repeat;
    private int repeatWait = 0;
    private Button sendBt;
    private boolean sendClear;
    private EditText sendEdit;
    private int sendWait = 0;
    private boolean sendloop = false;
    private final Handler tcpServerHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        KeeperMainActivity.this.remoteAddress = KeeperMainActivity.this.TcpSocket.getRemoteSocketAddress().toString();
                        KeeperMainActivity.this.localAddress = KeeperMainActivity.this.TcpSocket.getLocalSocketAddress().toString();
                        Log.d(KeeperMainActivity.TAG, "CONNECTEDAddress" + KeeperMainActivity.this.remoteAddress + "Port" + KeeperMainActivity.this.TcpSocket.getPort());
//                        KeeperMainActivity.this.ipText.setText(KeeperMainActivity.this.remoteAddress);
                        KeeperMainActivity.this.mtcpReadThread = new tcpReadThread();
                        KeeperMainActivity.this.mtcpReadThread.start();
                        KeeperMainActivity.this.connectState = 3;
//                        KeeperMainActivity.this.connectBt.setText("Disconnet");
                        KeeperMainActivity.this.messegeDisplay("Connect", KeeperMainActivity.this.remoteAddress);
                        return;
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = null;
                    if (KeeperMainActivity.this.hexMode) {
                        readMessage = Function.hexToAs(readBuf, msg.arg1);
                    } else {
                        try {
                            readMessage = new String(readBuf, 0, msg.arg1);
                        } catch (StringIndexOutOfBoundsException e2) {
                            e2.printStackTrace();
                        }
                    }
                    Log.d(KeeperMainActivity.TAG, "MESSAGE_READ" + readMessage);
                    if (readMessage != null && readMessage != null) {
                        if (KeeperMainActivity.this.markON) {
                            readMessage = "<<" + readMessage;
                            mConnectedThread.write(readMessage);
                            Log.d(KeeperMainActivity.TAG, "alhamdulillah masuk");

                        }
//                        KeeperMainActivity.this.messegeDisplay(readMessage, KeeperMainActivity.this.remoteAddress);

                        return;
                    }
                    return;
                case 3:
                    Log.d(KeeperMainActivity.TAG, "MESSAGE_CLOSE");
                    KeeperMainActivity.this.disconnect();
                    KeeperMainActivity.this.messegeDisplay("Disconnect", KeeperMainActivity.this.remoteAddress);
                    return;
                case 10:
                    KeeperMainActivity.this.recieveArrayAdapter.notifyDataSetChanged();
                    return;
                default:
                    return;
            }
        }
    };
    private String terminate;
    private boolean timeON;


    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address;
    ///////////////////////////////////////////////////////////////////////////////////////////////////

//    private static final String TAG = "OCVSample::Activity";
    //int send;
    private Mat zoomCorner;
    private boolean mIsColorSelected = false;
    private Mat mRgba;
    private Scalar mBlobColorRgba;
    private Scalar mBlobColorHsv;
    private ColorBlobDetector mDetector;
    private Mat mSpectrum;
    private Size SPECTRUM_SIZE;
    private Scalar CONTOUR_COLOR;
    private CameraBridgeViewBase mOpenCvCameraView;


    //////////////////////////////////////////////////
    // Batas pixel UNTUK DEFENDER
    private static final int BATAS_X_KUADRAN1 = 330; //-20
    private static final int BATAS_X_KUADRAN2 = 360; //+20
    private static final int BATAS_X_KUADRAN3 = 420; //-20
    private static final int BATAS_X_KUADRAN4 = 450; //+20

    private static final int BATAS_Y = 0;
    ///////////////////////////////////////////////////

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(KeeperMainActivity.this);

                    //frame resolution
                    mOpenCvCameraView.setMaxFrameSize(800, 480);
                    // Camera Resolution Support
                    // 160 x 90		// 160 x 120	// 176 x 144 	// 320 x 240	// 320 x 180
                    // 352 x 288 	// 432 x 240	// 640 x 480	// 640 x 360	// 800 x 480
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public KeeperMainActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        mOpenCvCameraView = findViewById(R.id.color_blob_detection_activity_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
//  mOpenCvCameraView.setCameraIndex(1);//0 for rear, 1 for front
        mOpenCvCameraView.enableFpsMeter();

        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();

        //////////////////
//        new Thread() {
//            public void run() {
//                KeeperMainActivity.this.connect();
//            }
//        }.start();

    }



    /* Access modifiers changed, original: protected */
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.localEchoON = sharedPreferences.getBoolean("localecho_preference", false);
        Log.d(TAG, "checkboxpreference" + this.localEchoON);
        this.dateON = sharedPreferences.getBoolean("date_preference", false);
        Log.d(TAG, "dateON" + this.dateON);
        this.timeON = sharedPreferences.getBoolean("time_preference", false);
        Log.d(TAG, "timeON" + this.timeON);
        this.ipAdressON = sharedPreferences.getBoolean("ipaddress_preference", false);
        Log.d(TAG, "ipAdressON" + this.ipAdressON);
        this.markON = sharedPreferences.getBoolean("mark_preference", false);
        Log.d(TAG, "markON" + this.markON);
        this.terminate = sharedPreferences.getString("terminate_preference", "NONE");
        try {
            this.lineNumber = Integer.parseInt(sharedPreferences.getString("scrollbuffer_preference", "60000"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            this.lineNumber = 60000;
        }
        Log.d(TAG, "lineNumber" + this.lineNumber);
        try {
            this.repeatWait = Integer.parseInt(sharedPreferences.getString("repeatwait_preference", "1"));
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            this.repeatWait = 1;
        }
        Log.d(TAG, "rpeatWait" + this.repeatWait);
        this.hexMode = sharedPreferences.getBoolean("binary_preference", false);
        this.sendClear = sharedPreferences.getBoolean("sendClear_preference", false);
//        this.portEdit.setText(getSharedPreferences("tcpServer", 0).getString("PORT", "4000"));
    }




    private void messegeDisplay(String message, String address) {
        message = Function.replaceCode(message);
        if (this.ipAdressON) {
            message = message + "(" + address + ")";
        }
        if (this.dateON || this.timeON) {
            this.currentTimeMillis = System.currentTimeMillis();
            this.cal = Calendar.getInstance();
            this.cal.setTimeInMillis(this.currentTimeMillis);
        }
        if (this.dateON) {
            int year = this.cal.get(1);
            int month = this.cal.get(2);
            message = message + ("[" + year + "/" + (month + 1) + "/" + this.cal.get(5) + "]");
        }
        if (this.timeON) {
            int hour = this.cal.get(11);
            int minute = this.cal.get(12);
            int second = this.cal.get(13);
            message = message + ("[" + hour + ":" + minute + ":" + second + "." + this.cal.get(14) + "]");
        }
        this.recieveArrayAdapter.add(message);
        this.posi = this.recieveArrayAdapter.getCount();
        Log.d(TAG, "count" + this.posi);
        if (this.posi > this.lineNumber) {
            this.posi = this.lineNumber;
            this.list.remove(0);
            this.recieveArrayAdapter.notifyDataSetChanged();
        }
        this.messageView.setSelection(this.posi);
    }



    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
        try {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    @Override
    public void onResume() {
        super.onResume();

        //Get MAC address from DeviceListActivity via intent
        Intent intent = getIntent();

        //Get the MAC address from the DeviceListActivty via EXTRA
        address = intent.getStringExtra(KeeperDevicesListActivity.EXTRA_DEVICE_ADDRESS);

        //create device and set the MAC address
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
        }
        // Establish the Bluetooth socket connection.
        try {
            btSocket.connect();
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();


        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        zoomCorner = new Mat(height, width, CvType.CV_8UC4);
        mDetector = new ColorBlobDetector();
        mSpectrum = new Mat();
        mBlobColorRgba = new Scalar(255);
        mBlobColorHsv = new Scalar(255);
        SPECTRUM_SIZE = new Size(200, 64);
        CONTOUR_COLOR = new Scalar(255, 0, 0, 255);
    }

    public void onCameraViewStopped() {
        //mRgba.release();                        //FOR EXIT WHILE TURN OFF SCREEN
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        if (this.connectState == 1) {
            this.connectState = 2;
//            this.connectBt.setText("Disconnet");
            new Thread() {
                public void run() {
                    KeeperMainActivity.this.connect();
                }
            }.start();
//            return;
        }
        mRgba = inputFrame.rgba();

        if (mIsColorSelected) {
            mDetector.process(mRgba);
            List<MatOfPoint> contours = mDetector.getContours();

            if (contours.size() == 0) {
                mConnectedThread.write("0");    // Send "0" via Bluetooth
            }

            for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
                // Minimum size allowed for consideration
                MatOfPoint2f approxCurve = new MatOfPoint2f();
                MatOfPoint2f contour2f = new MatOfPoint2f(contours.get(contourIdx).toArray());

                //Processing on mMOP2f1 which is in type MatOfPoint2f
                double approxDistance = Imgproc.arcLength(contour2f, true) * 0.02;
                Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);

                //Convert back to MatOfPoint
                MatOfPoint points = new MatOfPoint(approxCurve.toArray());

                // Get bounding rect of contour
                Rect rect = Imgproc.boundingRect(points);
                int rex = rect.x + rect.width / 2;
                int rey = rect.y + rect.height / 2;
                int q = 0;


                //FILTERING//
                if (contours.size() == 1) {

                    if (rect.width >= 40 && rect.height >= 40) {


                        rectangle(mRgba, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 1);

                        if (rex < BATAS_X_KUADRAN1 && rey > BATAS_Y) {
                            //q1
                            mConnectedThread.write("1");    // Send "1" via Bluetooth
                            q = 1;
                        }

                        if (rex >= BATAS_X_KUADRAN1 && rex < BATAS_X_KUADRAN2 && rey > BATAS_Y) {
                            //q2 (center)
                            mConnectedThread.write("2");    // Send "2" via Bluetooth
                            q = 2;
                        }

                        if (rex >= BATAS_X_KUADRAN2 && rex < BATAS_X_KUADRAN3 && rey > BATAS_Y) {
                            //q3
                            mConnectedThread.write("3");    // Send "3" via Bluetooth
                            q = 3;
                        }


                        if (rex >= BATAS_X_KUADRAN3 && rex < BATAS_X_KUADRAN4 && rey > BATAS_Y) {
                            //q3
                            mConnectedThread.write("4");    // Send "4" via Bluetooth
                            q = 4;
                        }


                        if (rex >= BATAS_X_KUADRAN4 && rey > BATAS_Y) {
                            //q3
                            mConnectedThread.write("5");    // Send "5" via Bluetooth
                            q = 5;
                        }


                        putText(mRgba, "KEEPER : alhamdulillah," +"tinggi :"+ rect.height +"lebar :"+ rect.height + " X : " + rex + " Y : " + rey + " q = " + q, new Point(30, 30), 1, 1, new Scalar(255, 0, 0, 255), 2);
                    }
                }

                if (contours.size() > 1) {

                    if (rect.width >= 40 && rect.height >= 40) {

                         rectangle(mRgba, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 1);

                        if (rex < BATAS_X_KUADRAN1 && rey > BATAS_Y) {
                            //q1
                            mConnectedThread.write("1");    // Send "1" via Bluetooth
                            q = 1;
                        }

                        if (rex >= BATAS_X_KUADRAN1 && rex < BATAS_X_KUADRAN2 && rey > BATAS_Y) {
                            //q2 (center)
                            mConnectedThread.write("2");    // Send "2" via Bluetooth
                            q = 2;
                        }

                        if (rex >= BATAS_X_KUADRAN2 && rex < BATAS_X_KUADRAN3 && rey > BATAS_Y) {
                            //q3
                            mConnectedThread.write("3");    // Send "3" via Bluetooth
                            q = 3;
                        }


                        if (rex >= BATAS_X_KUADRAN3 && rex < BATAS_X_KUADRAN4 && rey > BATAS_Y) {
                            //q3
                            mConnectedThread.write("4");    // Send "4" via Bluetooth
                            q = 4;
                        }


                        if (rex >= BATAS_X_KUADRAN4 && rey > BATAS_Y) {
                            //q3
                            mConnectedThread.write("5");    // Send "5" via Bluetooth
                            q = 5;
                        }


                        putText(mRgba, "KEEPER : alhamdulillah," +"tinggi :"+ rect.height +"lebar :"+ rect.height + " X : " + rex + " Y : " + rey + " q = " + q, new Point(30, 30), 1, 1, new Scalar(255, 0, 0, 255), 2);
                     }
                }
                //END FILTERING//
            }
        }

        return mRgba;
    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);
        return new Scalar(pointMatRgba.get(0, 0));
    }


    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if (btAdapter == null) {
            Toast.makeText(getBaseContext(), "Device does not support bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int cols = mRgba.cols();
        int rows = mRgba.rows();
        int xOffset = (mOpenCvCameraView.getWidth() - cols) / 2;
        int yOffset = (mOpenCvCameraView.getHeight() - rows) / 2;
        int x = (int) event.getX() - xOffset;
        int y = (int) event.getY() - yOffset;

        if ((x < 0) || (y < 0) || (x > cols) || (y > rows))
            return false;

        Rect touchedRect = new Rect();

        touchedRect.x = (x > 4) ? x - 4 : 0;
        touchedRect.y = (y > 4) ? y - 4 : 0;

        touchedRect.width = (x + 4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
        touchedRect.height = (y + 4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;

        Mat touchedRegionRgba = mRgba.submat(touchedRect);

        Mat touchedRegionHsv = new Mat();
        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        // Calculate average color of touched region
        mBlobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width * touchedRect.height;


        for (int i = 0; i < mBlobColorHsv.val.length; i++)
            mBlobColorHsv.val[i] /= pointCount;
        mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);
        mDetector.setHsvColor(mBlobColorHsv);
        mIsColorSelected = true;
        touchedRegionRgba.release();
        touchedRegionHsv.release();
        return false; // don't need subsequent touch events
    }

    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
            }
        }
    }

    public void connect() {
        try {
            int port = 8080;
            Log.d(TAG, " port " + port);
            this.TcpServerSocket = new ServerSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (NumberFormatException e3) {
            e3.printStackTrace();
        }
        if (this.TcpServerSocket != null) {
            try {
                this.TcpServerSocket.setSoTimeout(1000);
            } catch (SocketException e4) {
                e4.printStackTrace();
            }
            while (this.connectState == 2) {
                try {
                    this.TcpSocket = this.TcpServerSocket.accept();
                } catch (IOException e22) {
                    e22.printStackTrace();
                }
                if (this.TcpSocket != null && this.TcpSocket.isConnected()) {
                    Log.d(TAG, "connect");
                    this.connectState = 3;
                    this.tcpServerHandler.obtainMessage(1).sendToTarget();
                }
            }
        }
    }
    private void disconnect() {
        this.sendloop = false;
        this.connectState = 1;
        if (this.TcpSocket != null) {
            try {
                this.TcpSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (this.TcpServerSocket != null) {
            try {
                this.TcpServerSocket.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void close() {
        disconnect();
        SharedPreferences.Editor editor = getSharedPreferences("tcpServer", 0).edit();
        editor.putString("PORT", this.portEdit.getText().toString());
        editor.apply();
    }



    private class tcpReadThread extends Thread {
        private final InputStream tcpInStream;

        public tcpReadThread() {
            InputStream tmpIn = null;
            try {
                tmpIn = KeeperMainActivity.this.TcpSocket.getInputStream();
            } catch (IOException e) {
                Log.e(KeeperMainActivity.TAG, "temp sockets not created", e);
            }
            this.tcpInStream = tmpIn;
        }

        public void run() {
            super.run();
            byte[][] Rcv = (byte[][]) Array.newInstance(Byte.TYPE, new int[]{100, 4096});
            byte Index = (byte) 0;
            while (KeeperMainActivity.this.connectState == 3) {
                int readDataSize = 0;
                try {
                    readDataSize = this.tcpInStream.read(Rcv[Index]);
                } catch (IOException e) {
                    Log.d("TcpServerActivty.recv", e.toString());
                    e.printStackTrace();
                }
                if (readDataSize == -1) {
                    KeeperMainActivity.this.connectState = 1;
                    KeeperMainActivity.this.tcpServerHandler.obtainMessage(3).sendToTarget();
                } else if (readDataSize > 0) {
                    Log.d(KeeperMainActivity.TAG, "Rcv" + Rcv[Index]);
                    KeeperMainActivity.this.tcpServerHandler.obtainMessage(2, readDataSize, -1, Rcv[Index]).sendToTarget();
                    if (Index >= (byte) 99) {
                        Index = (byte) 0;
                    } else {
                        Index = (byte) (Index + 1);
                    }
                }
            }
        }
    }

}

