package e.wldn.bismillah;
/*this project created by : Muhammad Wildan Nur Karim
  More information about this project -> https:google.com */

//THIS IS ACTIVITY FOR STRIKER
//DIFFERENT BETWEEN STRIKER AND KEEPER PROGRAM ARE ON THE "OnCameraFrame"


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

import static org.opencv.imgproc.Imgproc.putText;
import static org.opencv.imgproc.Imgproc.rectangle;

public class TestActivity extends Activity implements View.OnTouchListener, CameraBridgeViewBase.CvCameraViewListener2 {
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    Handler bluetoothIn;

    int low_H = 45;
    int high_H = 75;
    int low_S = 10;
    int high_S = 255;
    int low_V = 10;
    int high_V = 255;
    Mat fieldcolor;

    Scalar fieldcolor1, fieldcolor2;

    final int handlerState = 0;                 //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address;
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String TAG = "OCVSample::Activity";
    //int send;
    private Mat zoomCorner;
    private boolean mIsColorSelected = false;
    private Mat mRgba;
    private Scalar mBlobColorRgba;
    private Scalar mBlobColorHsv;
    private ColorBlobDetector mDetector;
    private Mat mSpectrum;
    private Size SPECTRUM_SIZE;
    private Scalar CONTOUR_COLOR;
    private CameraBridgeViewBase mOpenCvCameraView;

    //////////////////////////////////////////////////
    // Batas pixel UNTUK STRIKER
    private static final int BATAS_X = 400;

    private static final int BATAS_SERONG_KIRI = 148;
    private static final int BATAS_SERONG_KANAN = 348;
    private static final int BATAS_TENGAH1 = 226;
    private static final int BATAS_TENGAH2 = 270;
    private static final int BATAS_Y_KUADRAN1 = 240;
///////////////////////////////////////////////////

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(TestActivity.this);

                    //frame resolution
                    mOpenCvCameraView.setMaxFrameSize(1080, 800);
                    // Camera Resolution Support
                    // 160 x 90		// 160 x 120	// 176 x 144 	// 320 x 240	// 320 x 180
                    // 352 x 288 	// 432 x 240	// 640 x 480	// 640 x 360	// 800 x 480
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public TestActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.test_layout);

        mOpenCvCameraView = findViewById(R.id.color_blob_detection_activity_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
//  mOpenCvCameraView.setCameraIndex(1);//0 for rear, 1 for front
        mOpenCvCameraView.enableFpsMeter();
        fieldcolor1 = new Scalar(low_H, low_S, low_V);
        fieldcolor2 = new Scalar(high_H, high_S, high_V);
    }


    @Override
    public void onPause() {
        super.onPause();
//        if (mOpenCvCameraView != null)
//            mOpenCvCameraView.disableView();
//        try {
//            //Don't leave Bluetooth sockets open when leaving activity
//            btSocket.close();
//        } catch (IOException e2) {
//            //insert code to deal with this
//        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
//    if (mOpenCvCameraView != null)
//      mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        fieldcolor = new Mat(width, height, CvType.CV_16UC4);
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mDetector = new ColorBlobDetector();
        mSpectrum = new Mat();
        mBlobColorRgba = new Scalar(255);
        mBlobColorHsv = new Scalar(255);
        SPECTRUM_SIZE = new Size(200, 64);
        CONTOUR_COLOR = new Scalar(255, 0, 0, 255);
    }

    public void onCameraViewStopped() {
        //mRgba.release();                        //FOR EXIT WHILE TURN OFF SCREEN
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        Core.inRange(mRgba, fieldcolor1, fieldcolor2, fieldcolor);

        double[] rgb_pojok_kiri_atas;
        double[] rgb_pojok_kiri_bawah;
        double[] rgb_pojok_kanan_atas;
        double[] rgb_pojok_kanan_bawah;
        mRgba = inputFrame.rgba();


        if (mIsColorSelected) {
            mDetector.process(mRgba);
            List<MatOfPoint> contours = mDetector.getContours();

            if (contours.size() == 1) {
                for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
                    // Minimum size allowed for consideration
                    MatOfPoint2f approxCurve = new MatOfPoint2f();
                    MatOfPoint2f contour2f = new MatOfPoint2f(contours.get(contourIdx).toArray());

                    //Processing on mMOP2f1 which is in type MatOfPoint2f
                    double approxDistance = Imgproc.arcLength(contour2f, true) * 0.02;
                    Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);

                    //Convert back to MatOfPoint
                    MatOfPoint points = new MatOfPoint(approxCurve.toArray());

                    // Get bounding rect of contour
                    Rect rect = Imgproc.boundingRect(points);
                    int rex = rect.x + rect.width / 2;          //koordinat x
                    int rey = rect.y + rect.height / 2;         //koordinat y

                    int LX_upper = rect.x + 1;                  //koordinat x pojok kiri atas
                    int LY_upper = rect.y + 1;                  //koordinat y pojok kiri atas

                    int LX_lower = rect.x + 1;                  //koordinat x pojok kiri bawah
                    int LY_lower = rect.y + rect.height - 1;    //koordinat y pojok kiri bawah

                    int RX_upper = rect.x + rect.width - 1;     //koordinat x pojok kanan atas
                    int RY_upper = rect.y + 1;                  //koordinat y pojok kanan atas

                    int RX_lower = rect.x + rect.width - 1;     //koordinat x pojok kanan bawah
                    int RY_lower = rect.y + rect.height - 1;    //koordinat y pojok kanan bawah

                    rgb_pojok_kiri_atas = mRgba.get(LY_upper, LX_upper);
                    rgb_pojok_kiri_bawah = mRgba.get(LY_lower, LX_lower);
                    rgb_pojok_kanan_atas = mRgba.get(RY_upper, RX_upper);
                    rgb_pojok_kanan_bawah = mRgba.get(RY_lower, RX_lower);


                    int q = 0;

                    //FILTERING//
                    if (contours.size() == 1) {
                        //semua pojok hijau
                        if (rgb_pojok_kanan_atas[0] >= 0 && rgb_pojok_kanan_atas[1] >= 0 && rgb_pojok_kanan_atas[2] >= 0 &&
                                rgb_pojok_kanan_atas[0] < 10 && rgb_pojok_kanan_atas[1] < 255 && rgb_pojok_kanan_atas[2] < 125 &&

                                rgb_pojok_kanan_bawah[0] >= 0 && rgb_pojok_kanan_bawah[1] >= 0 && rgb_pojok_kanan_bawah[2] >= 0 &&
                                rgb_pojok_kanan_bawah[0] < 10 && rgb_pojok_kanan_bawah[1] < 255 && rgb_pojok_kanan_bawah[2] < 125 &&

                                rgb_pojok_kiri_atas[0] >= 0 && rgb_pojok_kiri_atas[1] >= 0 && rgb_pojok_kiri_atas[2] >= 0 &&
                                rgb_pojok_kiri_atas[0] < 10 && rgb_pojok_kiri_atas[1] < 255 && rgb_pojok_kiri_atas[2] < 125 &&

                                rgb_pojok_kiri_bawah[0] >= 0 && rgb_pojok_kiri_bawah[1] >= 0 && rgb_pojok_kiri_bawah[2] >= 0 &&
                                rgb_pojok_kiri_bawah[0] < 10 && rgb_pojok_kiri_bawah[1] < 255 && rgb_pojok_kiri_bawah[2] < 125

                                ||

                                //semua pojok hijau (kecuali kanan atas)
                                rgb_pojok_kanan_atas[0] >= 70 && rgb_pojok_kanan_atas[1] >= 130 && rgb_pojok_kanan_atas[2] >= 123 &&
                                        rgb_pojok_kanan_atas[0] < 255 && rgb_pojok_kanan_atas[1] < 255 && rgb_pojok_kanan_atas[2] < 255 &&

                                        rgb_pojok_kanan_bawah[0] >= 0 && rgb_pojok_kanan_bawah[1] >= 0 && rgb_pojok_kanan_bawah[2] >= 0 &&
                                        rgb_pojok_kanan_bawah[0] < 10 && rgb_pojok_kanan_bawah[1] < 255 && rgb_pojok_kanan_bawah[2] < 125 &&

                                        rgb_pojok_kiri_atas[0] >= 0 && rgb_pojok_kiri_atas[1] >= 0 && rgb_pojok_kiri_atas[2] >= 0 &&
                                        rgb_pojok_kiri_atas[0] < 10 && rgb_pojok_kiri_atas[1] < 255 && rgb_pojok_kiri_atas[2] < 125 &&

                                        rgb_pojok_kiri_bawah[0] >= 0 && rgb_pojok_kiri_bawah[1] >= 0 && rgb_pojok_kiri_bawah[2] >= 0 &&
                                        rgb_pojok_kiri_bawah[0] < 10 && rgb_pojok_kiri_bawah[1] < 255 && rgb_pojok_kiri_bawah[2] < 125

                                ||

                                //semua pojok hijau (kecuali kanan bawah)
                                rgb_pojok_kanan_atas[0] >= 0 && rgb_pojok_kanan_atas[1] >= 0 && rgb_pojok_kanan_atas[2] >= 0 &&
                                        rgb_pojok_kanan_atas[0] < 10 && rgb_pojok_kanan_atas[1] < 255 && rgb_pojok_kanan_atas[2] < 125 &&

                                        rgb_pojok_kanan_bawah[0] >= 70 && rgb_pojok_kanan_bawah[1] >= 130 && rgb_pojok_kanan_bawah[2] >= 123 &&
                                        rgb_pojok_kanan_bawah[0] < 255 && rgb_pojok_kanan_bawah[1] < 255 && rgb_pojok_kanan_bawah[2] < 255 &&

                                        rgb_pojok_kiri_atas[0] >= 0 && rgb_pojok_kiri_atas[1] >= 0 && rgb_pojok_kiri_atas[2] >= 0 &&
                                        rgb_pojok_kiri_atas[0] < 10 && rgb_pojok_kiri_atas[1] < 255 && rgb_pojok_kiri_atas[2] < 125 &&

                                        rgb_pojok_kiri_bawah[0] >= 0 && rgb_pojok_kiri_bawah[1] >= 0 && rgb_pojok_kiri_bawah[2] >= 0 &&
                                        rgb_pojok_kiri_bawah[0] < 10 && rgb_pojok_kiri_bawah[1] < 255 && rgb_pojok_kiri_bawah[2] < 125
                                ||

                                //semua pojok hijau (kecuali kiri atas)
                                rgb_pojok_kanan_atas[0] >= 0 && rgb_pojok_kanan_atas[1] >= 0 && rgb_pojok_kanan_atas[2] >= 0 &&
                                        rgb_pojok_kanan_atas[0] < 10 && rgb_pojok_kanan_atas[1] < 255 && rgb_pojok_kanan_atas[2] < 125 &&

                                        rgb_pojok_kanan_bawah[0] >= 0 && rgb_pojok_kanan_bawah[1] >= 0 && rgb_pojok_kanan_bawah[2] >= 0 &&
                                        rgb_pojok_kanan_bawah[0] < 10 && rgb_pojok_kanan_bawah[1] < 255 && rgb_pojok_kanan_bawah[2] < 125 &&

                                        rgb_pojok_kiri_atas[0] >= 70 && rgb_pojok_kiri_atas[1] >= 130 && rgb_pojok_kiri_atas[2] >= 123 &&
                                        rgb_pojok_kiri_atas[0] < 255 && rgb_pojok_kiri_atas[1] < 255 && rgb_pojok_kiri_atas[2] < 255 &&

                                        rgb_pojok_kiri_bawah[0] >= 0 && rgb_pojok_kiri_bawah[1] >= 0 && rgb_pojok_kiri_bawah[2] >= 0 &&
                                        rgb_pojok_kiri_bawah[0] < 10 && rgb_pojok_kiri_bawah[1] < 255 && rgb_pojok_kiri_bawah[2] < 125

                                ||

                                //semua pojok hijau (kecuali kiri bawah)
                                rgb_pojok_kanan_atas[0] >= 0 && rgb_pojok_kanan_atas[1] >= 0 && rgb_pojok_kanan_atas[2] >= 0 &&
                                        rgb_pojok_kanan_atas[0] < 10 && rgb_pojok_kanan_atas[1] < 255 && rgb_pojok_kanan_atas[2] < 125 &&

                                        rgb_pojok_kanan_bawah[0] >= 0 && rgb_pojok_kanan_bawah[1] >= 0 && rgb_pojok_kanan_bawah[2] >= 0 &&
                                        rgb_pojok_kanan_bawah[0] < 10 && rgb_pojok_kanan_bawah[1] < 255 && rgb_pojok_kanan_bawah[2] < 125 &&

                                        rgb_pojok_kiri_atas[0] >= 0 && rgb_pojok_kiri_atas[1] >= 0 && rgb_pojok_kiri_atas[2] >= 0 &&
                                        rgb_pojok_kiri_atas[0] < 10 && rgb_pojok_kiri_atas[1] < 255 && rgb_pojok_kiri_atas[2] < 125 &&

                                        rgb_pojok_kiri_bawah[0] >= 70 && rgb_pojok_kiri_bawah[1] >= 130 && rgb_pojok_kiri_bawah[2] >= 123 &&
                                        rgb_pojok_kiri_bawah[0] < 255 && rgb_pojok_kiri_bawah[1] < 255 && rgb_pojok_kiri_bawah[2] < 255

                                ||

                                rgb_pojok_kanan_atas[0] >= 70 && rgb_pojok_kanan_atas[1] >= 130 && rgb_pojok_kanan_atas[2] >= 123 &&
                                        rgb_pojok_kanan_atas[0] < 255 && rgb_pojok_kanan_atas[1] < 255 && rgb_pojok_kanan_atas[2] < 255 &&

                                        rgb_pojok_kanan_bawah[0] >= 70 && rgb_pojok_kanan_bawah[1] >= 130 && rgb_pojok_kanan_bawah[2] >= 123 &&
                                        rgb_pojok_kanan_bawah[0] < 255 && rgb_pojok_kanan_bawah[1] < 255 && rgb_pojok_kanan_bawah[2] < 255 &&

                                        rgb_pojok_kiri_atas[0] >= 0 && rgb_pojok_kiri_atas[1] >= 0 && rgb_pojok_kiri_atas[2] >= 0 &&
                                        rgb_pojok_kiri_atas[0] < 10 && rgb_pojok_kiri_atas[1] < 255 && rgb_pojok_kiri_atas[2] < 125 &&

                                        rgb_pojok_kiri_bawah[0] >= 0 && rgb_pojok_kiri_bawah[1] >= 0 && rgb_pojok_kiri_bawah[2] >= 0 &&
                                        rgb_pojok_kiri_bawah[0] < 10 && rgb_pojok_kiri_bawah[1] < 255 && rgb_pojok_kiri_bawah[2] < 125




                        ) {

                            if (rey > BATAS_SERONG_KANAN && rex < BATAS_X) {
                                //q2
//                                mConnectedThread.write("2");    // Send "2" via Bluetooth
                                q = 2;
                            }

                            if (rey <= BATAS_SERONG_KANAN && rey > BATAS_TENGAH2 && rex > 188 && rex < BATAS_X) {
                                //serong kanan
//                                mConnectedThread.write("2");    // Send "2" via Bluetooth
                                q = 2;
                            }

                            if (rey <= BATAS_SERONG_KANAN && rey > BATAS_TENGAH2 && rex < 188) {
                                //serong kanan
//                                mConnectedThread.write("6");    // Send "6" via Bluetooth
                                q = 6;
                            }

                            if (rey <= BATAS_TENGAH2 && rey > BATAS_TENGAH1 && rex < BATAS_X) {
                                //q3 (center)
//                                mConnectedThread.write("3");    // Send "3" via Bluetooth
                                q = 3;
                            }

                            if (rey <= BATAS_TENGAH1 && rey > BATAS_SERONG_KIRI && rex < 188) {
                                //serong kiri
//                                mConnectedThread.write("7");    // Send "2" via Bluetooth
                                q = 7;
                            }

                            if (rey <= BATAS_TENGAH1 && rey > BATAS_SERONG_KIRI && rex > 188 && rex < BATAS_X) {
                                //q4
//                                mConnectedThread.write("4");    // Send "2" via Bluetooth
                                q = 4;
                            }

                            if (rey <= BATAS_SERONG_KIRI && rex < BATAS_X) {
                                //q4
//                                mConnectedThread.write("4");    // Send "4" via Bluetooth
                                q = 4;
                            }

                            if (rey > BATAS_Y_KUADRAN1 && rex > BATAS_X) {
                                //q1
//                                mConnectedThread.write("1");    // Send "5" via Bluetooth
                                q = 1;
                            }

                            if (rey <= BATAS_Y_KUADRAN1 && rex > BATAS_X) {
                                //CENTRE
//                                mConnectedThread.write("5");    // Send "3" via Bluetooth
                                q = 5;
                            }

                            rectangle(mRgba, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 1);

                            putText(mRgba, "RGB Values: R: " + rgb_pojok_kanan_atas[0] + "| " + "G: " + rgb_pojok_kanan_atas[1] + "| " + "B: " + rgb_pojok_kanan_atas[2], new Point(450, 10), 1, 1, new Scalar(255, 255, 255, 255), 1);
                            putText(mRgba, "RGB Values: R: " + rgb_pojok_kanan_bawah[0] + "| " + "G: " + rgb_pojok_kanan_bawah[1] + "| " + "B: " + rgb_pojok_kanan_bawah[2], new Point(450, 470), 1, 1, new Scalar(255, 255, 255, 255), 1);
                            putText(mRgba, "RGB Values: R: " + rgb_pojok_kiri_atas[0] + "| " + "G: " + rgb_pojok_kiri_atas[1] + "| " + "B: " + rgb_pojok_kiri_atas[2], new Point(10, 10), 1, 1, new Scalar(255, 255, 255, 255), 1);
                            putText(mRgba, "RGB Values: R: " + rgb_pojok_kiri_bawah[0] + "| " + "G: " + rgb_pojok_kiri_bawah[1] + "| " + "B: " + rgb_pojok_kiri_bawah[2], new Point(10, 470), 1, 1, new Scalar(255, 255, 255, 255), 1);


//                            putText(mRgba, "STRIKER : alhamdulillah," + " X : " + rex + " Y : " + rey + " q = " + q, new Point(rect.x, rect.y), 1, 1, new Scalar(255, 255, 255, 255), 1);
                        }
                    }
//
//                        if (rgb_pojok_kanan_bawah[0] == 0 && rgb_pojok_kanan_bawah[1] == 0 && rgb_pojok_kanan_bawah[2] == 0) {
//
//                            int kanan_bawah = 1;
//                        }
//
//                        if (rgb_pojok_kiri_atas[0] == 0 && rgb_pojok_kiri_atas[1] == 0 && rgb_pojok_kiri_atas[2] == 0) {
//
//                            int kiri_atas = 1;
//                        }
//
//                        if (rgb_pojok_kiri_bawah[0] == 0 && rgb_pojok_kiri_bawah[1] == 0 && rgb_pojok_kiri_bawah[2] == 0) {
//
//                            int kiri_bawah = 1;
//                        }

//                        rectangle(mRgba, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 1);
//
//                        putText(mRgba, "STRIKER : alhamdulillah," + "R: " + rgb_pojok_kiri_bawah[0] + "," + "G: " + rgb_pojok_kiri_bawah[1] + "," + "B: " + rgb_pojok_kiri_bawah[2] + ",", new Point(rect.x, rect.y), 1, 1, new Scalar(255, 255, 255, 255), 1);

                }

            }
            //END FILTERING//
        }


        return mRgba;
    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int cols = mRgba.cols();
        int rows = mRgba.rows();

        int xOffset = (mOpenCvCameraView.getWidth() - cols) / 2;
        int yOffset = (mOpenCvCameraView.getHeight() - rows) / 2;

        int x = (int) event.getX() - xOffset;
        int y = (int) event.getY() - yOffset;

        if ((x < 0) || (y < 0) || (x > cols) || (y > rows))
            return false;

        Rect touchedRect = new Rect();

        touchedRect.x = (x > 4) ? x - 4 : 0;
        touchedRect.y = (y > 4) ? y - 4 : 0;

        touchedRect.width = (x + 4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
        touchedRect.height = (y + 4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;

        Mat touchedRegionRgba = mRgba.submat(touchedRect);

        Mat touchedRegionHsv = new Mat();
        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        // Calculate average color of touched region
        mBlobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width * touchedRect.height;

        for (int i = 0; i < mBlobColorHsv.val.length; i++)
            mBlobColorHsv.val[i] /= pointCount;
        mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);
        mDetector.setHsvColor(mBlobColorHsv);
        mIsColorSelected = true;
        touchedRegionRgba.release();
        touchedRegionHsv.release();

        return false; // don't need subsequent touch events
    }


}
