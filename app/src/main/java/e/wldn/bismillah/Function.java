package e.wldn.bismillah;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ShareCompat.IntentBuilder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

public class Function {
    public static final int FILE_DELETE = 2;
    public static final int FILE_LOAD = 0;
    public static final int FILE_SHERE = 1;
    public static final int MESSAGE_LOAD = 10;
    private static final String TAG = "Function";
    private static String fileName;
    private static AlertDialog m_Dlg;

    public static byte[] asToHex(String asciiString) {
        int len = asciiString.length();
        int size = (int) Math.ceil(((double) len) / 2.0d);
        byte[] hexByte = new byte[size];
        for (int i = 0; i < size; i++) {
            if ((i * 2) + 2 <= len) {
                try {
                    hexByte[i] = (byte) Integer.parseInt(asciiString.substring(i * 2, (i * 2) + 2), 16);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            } else {
                hexByte[i] = (byte) Integer.parseInt(asciiString.substring(i * 2, (i * 2) + 1), 16);
            }
            Log.d("TAG", "HEX" + hexByte[i]);
        }
        return hexByte;
    }

    public static String hexToAs(byte[] bytes, int size) {
        StringBuffer strbuf = new StringBuffer(size * 2);
        for (int index = 0; index < size; index++) {
            strbuf.append(Integer.toHexString(bytes[index] & 255));
        }
        return strbuf.toString();
    }

    public static boolean fileSave(final Activity activity, final ArrayList<String> stringList, final int size, String title) {
        final EditText editView = new EditText(activity);
//        new Builder(activity).setIcon(17301659).setTitle("Input " + title + "File Name").setView(editView).setPositiveButton("OK", new OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                Function.saveFunction(activity, stringList, size, editView.getText().toString());
//            }
//        }).setNegativeButton("CANCEL", new OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//            }
//        }).show();
        return true;
    }

    public static boolean saveFunction(Activity activity, ArrayList<String> stringList, int size, String title) {
        try {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(activity.openFileOutput(title + ".txt", 0), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < size; i++) {
                try {
                    writer.write((String) stringList.get(i));
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                try {
                    writer.newLine();
                } catch (IOException e22) {
                    e22.printStackTrace();
                }
            }
            try {
                writer.close();
            } catch (IOException e222) {
                e222.printStackTrace();
            }
            return true;
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static int fileLoad(Activity activity, ArrayList<String> stringList, String title) {
        Log.d(TAG, "fileSelect" + title);
        try {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(activity.openFileInput(title), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int i = 0;
            while (true) {
                try {
                    String tmp = reader.readLine();
                    if (tmp != null) {
                        stringList.add(tmp);
                        i++;
                    } else {
                        try {
                            reader.close();
                            return i;
                        } catch (IOException e2) {
                            e2.printStackTrace();
                            return i;
                        }
                    }
                } catch (IOException e22) {
                    e22.printStackTrace();
                    return -1;
                }
            }
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            return -1;
        } catch (NullPointerException e4) {
            e4.printStackTrace();
            return -1;
        }
    }

    public static int fileshare(Activity activity, ArrayList<String> arrayList, String title) {
        Log.d(TAG, "fileSelect" + title);
        try {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(activity.openFileInput(title), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String fileString = null;
            IntentBuilder builder = IntentBuilder.from(activity);
            builder.startChooser();
            int i = 0;
            while (true) {
                try {
                    String tmp = reader.readLine();
                    if (tmp != null) {
                        if (i == 0) {
                            fileString = tmp;
                        } else {
                            fileString = fileString + tmp + "\r\n";
                        }
                        i++;
                    } else {
//                        try {
//                            break;
//                        } catch (IOException e2) {
//                            e2.printStackTrace();
//                        }
                    }
                } catch (IOException e22) {
                    e22.printStackTrace();
                    return -1;
                }
            }
            //reader.close();
            //builder.setChooserTitle((CharSequence) "Choose Share App");
            //builder.setSubject(title);
            //Uri uri = Uri.fromFile(activity.getFileStreamPath(title));
            //Intent sendIntent = new Intent();
//            sendIntent.setAction("android.intent.action.SEND");
//            sendIntent.putExtra("android.intent.extra.TEXT", fileString);
//            sendIntent.setType("text/plain");
//            activity.startActivity(sendIntent);
//            return i;
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            return -1;
        } catch (NullPointerException e4) {
            e4.printStackTrace();
            return -1;
        }
    }

    public static void fileControl(final Activity activity, final ArrayList<String> stringList, final int ope, final Handler handler, String title) {
        ArrayList<String> fileArray = new ArrayList(Arrays.asList(activity.fileList()));
        ListView fileView = new ListView(activity);
        fileView.setAdapter(new ArrayAdapter(activity, 17367043, fileArray));
        fileView.setScrollingCacheEnabled(false);
        fileView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> items, View view, int position, long id) {
                Function.fileName = (String) ((ListView) items).getItemAtPosition(position);
                switch (ope) {
                    case 0:
                        Function.fileLoad(activity, stringList, Function.fileName);
                        handler.obtainMessage(10).sendToTarget();
                        break;
                    case 1:
                        Function.fileshare(activity, stringList, Function.fileName);
                        break;
                    case 2:
                        activity.deleteFile(Function.fileName);
                        break;
                }
                Function.m_Dlg.dismiss();
                Log.d(Function.TAG, "fileName" + Function.fileName);
            }
        });
        m_Dlg = new Builder(activity).setTitle("Select a " + title + "file").setPositiveButton("Cancel", null).setView(fileView).create();
        m_Dlg.show();
    }

    static String replaceCode(String str) {
        String resultStr = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 10) {
                resultStr = resultStr + "<LF>";
            } else if (str.charAt(i) == 13) {
                resultStr = resultStr + "<CR>";
            } else {
                resultStr = resultStr + str.charAt(i);
            }
        }
        return resultStr;
    }
}
